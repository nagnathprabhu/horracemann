var app = angular.module("myApp", ['ngRoute']);

app.controller("myCtrl", function($scope) {
    $scope.title = "Horace Mann";
    $scope.searchText;
    $scope.flag=false;
    
    $scope.myFunction=function(){
    	$scope.flag=true;
    	
    }
   
});

app.config(["$routeProvider",function($routeProvider) {
    $routeProvider
    .when("/", {
        templateUrl : "partials/main.html",
        controller : "myCtrl"
    })
    .when("/about",{
    	templateUrl: "partials/about.html"
    })
    .when("/admissions",{
    	templateUrl: "partials/admissions.html"
    })
    .when("/curriculum",{
    	templateUrl: "partials/curriculum.html"
    })
    .otherwise({
    	redirectTo: "/",
    })
}]);
