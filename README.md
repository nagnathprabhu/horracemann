# About the project #

A simple webpage that demonstrates AngularJS 1.6.4 routing and two-way binding .

### Environment ###

* HTML
* CSS
* JavaScript
* Angular JS 1.6.4
* Http Server


### How do I get set up? ###

* Clone the repo and navigate to the directory that contains the source
* Open a terminal at this location
* Now type `npm install` to install all the dependencies
* Now in the same command window type `http-server -c -1` to start running your application.
